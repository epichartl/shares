#!/bin/bash

mkdir -p /home/ubuntu/opt
mkdir -p /home/ubuntu/logs

curl -o /home/ubuntu/opt/miniconda.sh https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash /home/ubuntu/opt/miniconda.sh -b -p /home/ubuntu/miniconda 2>&1 > /home/ubuntu/logs/miniconda.inst
curl -o /home/ubuntu/opt/scvi2020.yml "https://gitlab.com/epichartl/shares/-/raw/main/conda_envs/scvi2020.yml?inline=false"
export PATH="/home/ubuntu/miniconda/bin:${PATH}"
conda init bash
source $HOME/.bashrc
conda update conda
conda env create --file /home/ubuntu/opt/scvi2020.yml 2>&1 | tee /home/ubuntu/inst.log
