#!/bin/bash
# aws setup script
apt-get upgrade -y
apt-get update -y
curl -o /home/ubuntu/setup.bash "https://gitlab.com/epichartl/shares/-/raw/main/conda_envs/scvi2020_hook.bash?inline=false"
chmod 777 /home/ubuntu/setup.bash
cd /home/ubuntu
runuser -l ubuntu -c "bash setup.bash"
